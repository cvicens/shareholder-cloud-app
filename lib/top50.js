var $fh = require('fh-mbaas-api');
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var db = require('./db-store');

var TOP50_SERVICE_GUID = process.env.TOP50_SERVICE_GUID || 'TOP50_SERVICE_GUID';
var TOP50_COLLECTION_NAME = process.env.TOP50_COLLECTION_NAME || "top50";
var TOP50_SERVICE_MOCKED_UP = process.env.TOP50_SERVICE_MOCKED_UP || "true";
var RHM_TOKEN_HEADER_NAME = process.env.RHM_TOKEN_HEADER_NAME || "x-fh-sessiontoken";

function _fetchTop50MockedUp (filter) {
  return new Promise(function(resolve, reject) {
    db.list(TOP50_COLLECTION_NAME, filter, function (err, data) {
      if (err) {
        reject({result:'ERROR', msg: err});
      } else {
        resolve(data);
      }
    });
  });
}

function _fetchTop50(filter, token) {
  return new Promise(function(resolve, reject) {
    var path = '/top50';
    console.log('path: ' + path);

    let headers = {};
    headers[RHM_TOKEN_HEADER_NAME] = token;
    $fh.service({
      "guid" : TOP50_SERVICE_GUID, 
      "path": path + '/' + filter.eq.regId, 
      "method": "GET", 
      "timeout": 25000,
      //"params": filter,
      "headers" : headers
    }, function(err, body, response) {
      console.log('statuscode: ', response && response.statusCode);
      if (err) {
        // An error occurred during the call to the service. log some debugging information
        console.log(path + ' service call failed - err : ', err);
        reject({result:'ERROR', msg: err});
      } else {
        resolve(body);
      }
    });
  });
}

function fetchTop50(filter, token) {
  console.log('fetchTop50 filter:', filter);
  console.log('TOP50_SERVICE_MOCKED_UP', TOP50_SERVICE_MOCKED_UP);
  if (TOP50_SERVICE_MOCKED_UP === 'true') {
    console.log('_fetchTop50MockedUp');
    return _fetchTop50MockedUp(filter);
  } else {
    console.log('_fetchTop50');
    return _fetchTop50(filter, token);
  }
}

function getIsoDate (date) {
  if (!date) {
    return null;
  }
  return date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2);
}

function route() {
  var router = new express.Router();
  router.use(cors());
  router.use(bodyParser.json());
  router.use(bodyParser.urlencoded({ extended: true }));

  router.get('/', function(req, res) {
    var id = req.query.id;
    console.log('id ' + id);
    if (typeof id === 'undefined' || id === '') {
      res.status(404).json([]);
      return;
    }
    db.read(TOP50_COLLECTION_NAME, id, function (err, data) {
      if (err) {
        res.status(500).json({result:'ERROR', msg: err})
      } else {
        res.status(200).json(data);
      }
    });
  });

  router.post('/', function(req, res) {
    var filter = req.body;
    console.log('filter: ' + filter);
    if (typeof filter === 'undefined') {
      res.status(404).json([]);
      return;
    }

    fetchTop50(filter).
    then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      res.status(500).json({result:'ERROR', msg: err})
    });
  });

  // Fetch TOP50 by regId
  router.get('/:regId', function(req, res) {
    let token = req.headers[RHM_TOKEN_HEADER_NAME];
    
    var regId = req.params.regId;
    console.log('Find TOP50 by regId', regId);
    if (typeof regId === 'undefined' || regId == '') {
      res.status(400).json([]);
    }
    
    var filter = {
      "eq": {
        "regId": regId
      },
      "sort": {
        "shares": -1
      }
    };
    fetchTop50(filter, token).
    then(function (data) {
      let result = {
        status : "ok",
        statusCode : 200,
        top50 : data
      };
      res.status(200).json(result);
    })
    .catch(function (err) {
      res.status(500).json({result:'ERROR', msg: err})
    });
  });

  return router;
}

module.exports = route;
